﻿using TaxCalculator.Contracts;

namespace TaxCalculator.Implementations.Items
{
    internal class ImportTaxDecorator : TaxDecorator
    {
        protected override decimal TaxRate { get; } = 0.05m;

        public ImportTaxDecorator(IItem item) : base(item)
        {
        }
    }
}

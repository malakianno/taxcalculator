﻿using TaxCalculator.Contracts;

namespace TaxCalculator.Implementations.Items
{
    internal class SalesTaxDecorator : TaxDecorator
    {
        protected override decimal TaxRate { get; } = 0.1m;

        public SalesTaxDecorator(IItem item) : base(item)
        {
        }
    }
}

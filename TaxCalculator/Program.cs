﻿using System;
using System.Collections.Generic;
using TaxCalculator.Contracts;
using TaxCalculator.Enums;
using TaxCalculator.Implementations;
using TaxCalculator.Implementations.Items;
using TaxCalculator.Implementations.TaxCalculators;
using TaxCalculator.Models;
using TaxCalculator.Services;

namespace TaxCalculator
{
    internal class Program
    {
        private static readonly ITaxCalculator _taxCalculator = new CombinedTaxCalculator(
            new List<ITaxCalculator> {
                new BasicSalesTaxCalculator(new[] { ItemType.Candy, ItemType.Coffee, ItemType.Popcorn}),
                new ImportSalesTaxCalculator(null)
            }
        );
        private static readonly ReceiptProvider _receiptProvider = new ReceiptProvider(_taxCalculator);
        private const string DecimalFormat = "0.##";

        internal static void Main(string[] args)
        {
            var order1 = new ShoppingBasket(_taxCalculator, _receiptProvider);
            order1.AddItem(new Item("16lb bag of Skittles", ItemType.Candy, 16m, false), 1);
            order1.AddItem(new Item("Walkman", ItemType.Other, 99.99m, false), 1);
            order1.AddItem(new Item("bag of microwave Popcorn", ItemType.Popcorn, 0.99m, false), 1);
            PrintReceipt(order1.GetReceipt());

            var order2 = new ShoppingBasket(_taxCalculator, _receiptProvider);
            order2.AddItem(new Item("bag of Vanilla-Hazelnut Coffee", ItemType.Coffee, 11m, true), 1);
            order2.AddItem(new Item("Vespa", ItemType.Other, 15001.25m, true), 1);
            PrintReceipt(order2.GetReceipt());

            var order3 = new ShoppingBasket(_taxCalculator, _receiptProvider);
            order3.AddItem(new Item("crate of Almond Snickers", ItemType.Candy, 75.99m, true), 1);
            order3.AddItem(new Item("Discman", ItemType.Other, 55m, false), 1);
            order3.AddItem(new Item("Bottle of Wine", ItemType.Other, 10m, true), 1);
            order3.AddItem(new Item("300# bag of Fair-Trade Coffee", ItemType.Coffee, 997.99m, false), 1);
            PrintReceipt(order3.GetReceipt());
        }

        private static void PrintReceipt(Receipt receipt)
        {
            foreach (var itemsGroup in receipt.ItemsGroups)
            {
                Console.WriteLine($"{itemsGroup.Items.Count} {itemsGroup.Description}: {itemsGroup.Price.ToString(DecimalFormat)}");
            }
            Console.WriteLine($"Sales Taxes: {receipt.SalesTaxes.ToString(DecimalFormat)}");
            Console.WriteLine($"Total: {receipt.Total.ToString(DecimalFormat)}");
            Console.WriteLine();
        }
    }
}

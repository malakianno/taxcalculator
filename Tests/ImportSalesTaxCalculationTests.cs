﻿using System.Linq;
using NUnit.Framework;
using TaxCalculator.Enums;
using TaxCalculator.Implementations.Items;
using TaxCalculator.Implementations.TaxCalculators;

namespace Tests
{
    [TestFixture]
    public class ImportSalesTaxCalculationTests
    {
        [Test]
        public void Test_Applying_Of_Import_Sales_Tax_To_One_Item_Success()
        {
            var item = new ImportTaxDecorator(new Item("Tchibo", ItemType.Coffee, 10m, false));
            var priceWithTax = item.GetPrice();
            Assert.AreEqual(10.5m, priceWithTax, "Basic sales tax wasn't applied properly.");
        }

        [Test]
        public void Test_Filtering_Of_Items_In_Import_Sales_Tax_Calculator_When_Item_Is_Exception_Success()
        {
            var item = new Item("Mars", ItemType.Candy, 10m, false);
            var calculator = new ImportSalesTaxCalculator(new[] { ItemType.Candy });

            var taxedItem = calculator.Calculate(new[] { item }).First();

            Assert.IsInstanceOf<Item>(taxedItem, "Unexpected item type has been returned.");
        }

        [Test]
        public void Test_Filtering_Of_Items_In_Import_Sales_Tax_Calculator_When_Item_Is_Not_Imported_Success()
        {
            var item = new Item("Mars", ItemType.Candy, 10m, false);
            var calculator = new ImportSalesTaxCalculator(null);

            var taxedItem = calculator.Calculate(new[] { item }).First();

            Assert.IsInstanceOf<Item>(taxedItem, "Unexpected item type has been returned.");
        }

        [Test]
        public void Test_Filtering_Of_Items_In_Basic_Sales_Tax_Calculator_When_There_Are_No_Exceptions_Success()
        {
            var item = new Item("Tchibo", ItemType.Coffee, 10m, true);
            var calculator = new ImportSalesTaxCalculator(null);

            var taxedItem = calculator.Calculate(new[] { item }).First();

            Assert.IsInstanceOf<ImportTaxDecorator>(taxedItem, "Unexpected item type has been returned.");
        }
    }
}

﻿namespace TaxCalculator.Enums
{
    internal enum ItemType
    {
        Other,
        Candy,
        Popcorn,
        Coffee
    }
}

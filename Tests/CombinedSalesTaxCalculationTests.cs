﻿using NUnit.Framework;
using TaxCalculator.Enums;
using TaxCalculator.Implementations.Items;

namespace Tests
{
    [TestFixture]
    public class CombinedSalesTaxCalculationTests
    {
        [Test]
        [Category("IntegrationTest")]
        public void Test_Applying_Of_Basic_And_Import_Sales_Taxes_For_Item_Success()
        {
            var item = new ImportTaxDecorator(new SalesTaxDecorator(new Item("Tchibo", ItemType.Coffee, 10m, true)));
            var priceWithTax = item.GetPrice();
            Assert.AreEqual(11.5m, priceWithTax, "Basic sales tax wasn't applied properly.");
        }

        [Test]
        [Category("IntegrationTest")]
        public void Test_Applying_Of_Basic_And_Import_Sales_Taxes_With_Rounding_Off_Value_For_Item_Success()
        {
            var item = new ImportTaxDecorator(new SalesTaxDecorator(new Item("Tchibo", ItemType.Coffee, 9.99m, true)));
            var priceWithTax = item.GetPrice();
            Assert.AreEqual(11.49m, priceWithTax, "Basic sales tax wasn't applied properly.");
        }
    }
}

﻿using TaxCalculator.Contracts;
using TaxCalculator.Enums;
using TaxCalculator.Utils;

namespace TaxCalculator.Implementations.Items
{
    internal abstract class TaxDecorator : IItem
    {
        public string Name => _item.Name;
        public decimal PreTaxPrice => _item.PreTaxPrice;
        public ItemType Type => _item.Type;
        public bool IsImported => _item.IsImported;
        protected readonly IItem _item;
        protected abstract decimal TaxRate { get; }
        private decimal? _price;

        public TaxDecorator(IItem item)
        {
            _item = item;
        }

        public string GetDescription() => _item.GetDescription();

        public decimal GetPrice()
        {
            if (!_price.HasValue)
            {
                _price = _item.GetPrice() + TaxUtil.RoundUp(TaxRate * _item.PreTaxPrice);
            }
            return _price.Value;
        }
    }
}

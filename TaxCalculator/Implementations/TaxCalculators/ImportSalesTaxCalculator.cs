﻿using System.Collections.Generic;
using System.Linq;
using TaxCalculator.Contracts;
using TaxCalculator.Enums;
using TaxCalculator.Implementations.Items;

namespace TaxCalculator.Implementations.TaxCalculators
{
    internal class ImportSalesTaxCalculator : ITaxCalculator
    {
        private readonly IEnumerable<ItemType> _exceptions;

        public ImportSalesTaxCalculator(IEnumerable<ItemType> exceptions)
        {
            _exceptions = exceptions;
        }

        public ICollection<IItem> Calculate(ICollection<IItem> items)
        {
            var result = new List<IItem>(items.Count);
            foreach (var item in items)
            {
                if (item.IsImported && (_exceptions == null || !_exceptions.Any(x => x == item.Type)))
                {
                    result.Add(new ImportTaxDecorator(item));
                }
                else
                {
                    result.Add(item);
                }
            }
            return result;
        }
    }
}

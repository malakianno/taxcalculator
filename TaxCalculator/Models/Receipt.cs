﻿using System.Collections.Generic;

namespace TaxCalculator.Models
{
    internal class Receipt
    {
        public IEnumerable<ItemsGroup> ItemsGroups { get; set; }
        public decimal SalesTaxes { get; set; }
        public decimal PriceWithoutTaxes { get; set; }
        public decimal Total { get; set; }
    }
}

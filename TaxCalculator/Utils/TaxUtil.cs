﻿using System;

namespace TaxCalculator.Utils
{
    internal class TaxUtil
    {
        public static decimal RoundUp(decimal tax)
        {
            // round up the price to the nearest number that is divisible by 0.05
            return Math.Ceiling(tax / 0.05m) * 0.05m;
        }
    }
}

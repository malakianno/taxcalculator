﻿using System.Collections.Generic;
using System.Linq;
using TaxCalculator.Contracts;
using TaxCalculator.Models;

namespace TaxCalculator.Services
{
    internal class ReceiptProvider
    {
        private readonly ITaxCalculator _taxCalculator;

        public ReceiptProvider(ITaxCalculator taxCalculator)
        {
            _taxCalculator = taxCalculator;
        }

        public Receipt Create(ICollection<IItem> items)
        {
            var taxedItems = _taxCalculator.Calculate(items);
            var receipt = new Receipt()
            {
                ItemsGroups = taxedItems
                .GroupBy(i => i.GetDescription())
                .Select(group =>
                {
                    var groupItems = group.Select(o => o).ToList();
                    var itemsGroup = new ItemsGroup()
                    {
                        Description = group.Key,
                        Items = groupItems,
                        Price = groupItems.Sum(o => o.GetPrice())
                    };
                    return itemsGroup;
                }),
                PriceWithoutTaxes = items.Sum(o => o.PreTaxPrice)
            };
            receipt.Total = receipt.ItemsGroups.Sum(o => o.Price);
            receipt.SalesTaxes = receipt.Total - receipt.PriceWithoutTaxes;
            return receipt;
        }
    }
}

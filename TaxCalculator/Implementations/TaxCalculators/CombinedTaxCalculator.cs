﻿using System.Collections.Generic;
using TaxCalculator.Contracts;

namespace TaxCalculator.Implementations.TaxCalculators
{
    internal class CombinedTaxCalculator : ITaxCalculator
    {
        private readonly IEnumerable<ITaxCalculator> _taxCalculators;

        public CombinedTaxCalculator(IEnumerable<ITaxCalculator> taxCalculators)
        {
            _taxCalculators = taxCalculators;
        }

        public ICollection<IItem> Calculate(ICollection<IItem> items)
        {
            var taxedItems = items;
            foreach (var taxCalculator in _taxCalculators)
            {
                taxedItems = taxCalculator.Calculate(taxedItems);
            }
            return taxedItems;
        }
    }
}

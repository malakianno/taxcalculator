﻿using System.Collections.Generic;
using TaxCalculator.Contracts;
using TaxCalculator.Models;
using TaxCalculator.Services;

namespace TaxCalculator.Implementations
{
    internal class ShoppingBasket
    {
        private List<IItem> _items = new List<IItem>();
        private readonly ITaxCalculator _taxCalculator;
        private readonly ReceiptProvider _receiptProvider;

        public ShoppingBasket(ITaxCalculator taxCalculator, ReceiptProvider receiptProvider)
        {
            _taxCalculator = taxCalculator;
            _receiptProvider = receiptProvider;
        }

        public void AddItem(IItem item, uint quantity)
        {
            for (var i = 0; i < quantity; i++)
            {
                _items.Add(item);
            }
        }

        public Receipt GetReceipt()
        {
            return _receiptProvider.Create(_items);
        }
    }
}

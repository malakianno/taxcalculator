﻿using System.Collections.Generic;
using TaxCalculator.Contracts;

namespace TaxCalculator.Models
{
    internal class ItemsGroup
    {
        public string Description { get; set; }
        public ICollection<IItem> Items { get; set; }
        public decimal Price { get; set; }
    }
}

﻿using TaxCalculator.Enums;

namespace TaxCalculator.Contracts
{
    internal interface IItem
    {
        string Name { get; }
        decimal PreTaxPrice { get; }
        ItemType Type { get; }
        bool IsImported { get; }
        string GetDescription();
        decimal GetPrice();
    }
}

﻿using System;
using System.IO;
using NUnit.Framework;
using TaxCalculator;

namespace Tests
{
    [TestFixture]
    internal class AcceptanceTests
    {
        [Test]
        public void AcceptanceTest_Success()
        {
            using (var sw = new StringWriter())
            {
                Console.SetOut(sw);
                Program.Main(new string[0]);
                Assert.AreEqual(ExpectedOutput, sw.ToString());
            }
        }

        private const string ExpectedOutput = @"1 16lb bag of Skittles: 16
1 Walkman: 109,99
1 bag of microwave Popcorn: 0,99
Sales Taxes: 10
Total: 126,98

1 imported bag of Vanilla-Hazelnut Coffee: 11,55
1 imported Vespa: 17251,5
Sales Taxes: 2250,8
Total: 17263,05

1 imported crate of Almond Snickers: 79,79
1 Discman: 60,5
1 imported Bottle of Wine: 11,5
1 300# bag of Fair-Trade Coffee: 997,99
Sales Taxes: 10,8
Total: 1149,78

";
    }
}

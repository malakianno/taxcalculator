﻿using System;
using NUnit.Framework;
using TaxCalculator.Utils;

namespace Tests
{
    [TestFixture]
    public class TaxUtilTests
    {
        [Test]
        [TestCase(0d, 0d)]
        [TestCase(0.01d, 0.05d)]
        [TestCase(1d, 1d)]
        [TestCase(-1d, -1d)]
        [TestCase(6.29564d, 6.3d)]
        public void Test_Rounding_Up_Of_Tax_Success(double inputDouble, double expectedDouble)
        {
            var expected = Convert.ToDecimal(expectedDouble);
            var actual = TaxUtil.RoundUp(Convert.ToDecimal(inputDouble));
            Assert.AreEqual(expectedDouble, actual);
        }
    }
}

﻿using TaxCalculator.Contracts;
using TaxCalculator.Enums;

namespace TaxCalculator.Implementations.Items
{
    internal class Item : IItem
    {
        public string Name { get; }
        public decimal PreTaxPrice { get; }
        public ItemType Type { get; }
        public bool IsImported { get; }

        public Item(string name, ItemType type, decimal preTaxPrice, bool isImported)
        {
            Name = name;
            Type = type;
            IsImported = isImported;
            PreTaxPrice = preTaxPrice;
        }

        public string GetDescription()
        {
            return $"{(IsImported ? "imported " : "")}{Name}";
        }

        public decimal GetPrice()
        {
            return PreTaxPrice;
        }
    }
}

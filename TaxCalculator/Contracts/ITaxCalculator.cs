﻿using System.Collections.Generic;

namespace TaxCalculator.Contracts
{
    internal interface ITaxCalculator
    {
        ICollection<IItem> Calculate(ICollection<IItem> items);
    }
}
